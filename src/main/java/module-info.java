module fr.passward {
    requires transitive javafx.controls;
    requires javafx.fxml;
    requires com.google.gson;
    requires commons.codec;

    opens fr.passward to javafx.fxml;
    opens fr.passward.controller to javafx.fxml,com.google.gson;
    opens fr.passward.model to com.google.gson;
    

    exports fr.passward;
    exports fr.passward.model;
    exports fr.passward.controller;

}
