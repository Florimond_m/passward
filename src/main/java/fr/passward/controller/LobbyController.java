package fr.passward.controller;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class LobbyController {

    public static final String LOBBY_VIEW_PATH = "/fr/passward/lobby.fxml";
    @FXML
    private TextField passwordFieldConnexion;
    @FXML
    private TextField emailFieldConnexion;
    @FXML
    private Button connexionButton;
    // private Pane mainPane;

    public void switchToPasswordManagerView() throws IOException {
        // Charge la vue PasswordManager
        FXMLLoader loader = new FXMLLoader(
                getClass().getResource(PasswordManagerController.PASSWORD_MANAGER_VIEW_PATH));
        Parent root = loader.load();

        // Obtient la référence du contrôleur PasswordManager
        //PasswordManagerController passwordManagerController = loader.getController();

        // Affiche la fenêtre du gestionnaire
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();

        // Ferme la fenêtre du lobby
        Stage lobbyStage = (Stage) connexionButton.getScene().getWindow();
        lobbyStage.close();
    }

    public void connexionButtonClicked(MouseEvent mouseEvent) throws IOException {
        // Vérifie les informations de connexion
        // ...

        // Charge la vue PasswordManager si les informations sont valides
        switchToPasswordManagerView();
    }
    // ...
}
