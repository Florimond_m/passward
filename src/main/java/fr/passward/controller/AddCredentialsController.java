package fr.passward.controller;

import fr.passward.model.Credentials;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.codec.digest.DigestUtils;

import fr.passward.controller.LobbyController;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.StringConverter;

public class AddCredentialsController {
    private static PasswordManagerController passwordManagerController;

    public static final String ADD_CREDENTIALS_VIEW_PATH = "/fr/passward/addCredentials.fxml";
    @FXML
    private TextField siteField;
    @FXML
    private TextField loginField;
    @FXML
    private TextField pwField;
    @FXML
    private TextField nbCharField;
    @FXML
    private TextField nbNumberField;
    @FXML
    private TextField nbSpecCharField;
    @FXML
    private CheckBox pwCheck;
    @FXML
    private CheckBox randomCheck;
    @FXML
    private CheckBox majCheck;
    @FXML
    private CheckBox minCheck;
    @FXML
    private TextArea resultField;
    @FXML
    private TextArea resultFieldNoHash;
    @FXML
    private Button backButton;
    @FXML
    private Label lblWelcome;

    public static File credentialsFile = new File("credentials_save.json");

    public void setPasswordManagerController(PasswordManagerController passwordManagerController) {
        this.passwordManagerController = passwordManagerController;
    }

    public void initialize() {

        
        randomCheck.selectedProperty().addListener((observable, oldValue, newValue) -> {
            nbCharField.setDisable(!newValue);
            nbNumberField.setDisable(!newValue);
            nbSpecCharField.setDisable(!newValue);
            majCheck.setDisable(!newValue);
            minCheck.setDisable(!newValue);
        });
        randomCheck.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                pwCheck.setSelected(false);
                pwField.setDisable(!newValue);
                nbCharField.clear();
                nbSpecCharField.clear();
                nbNumberField.clear();
            }
        });

        pwCheck.selectedProperty().addListener((observable, oldValue, newValue) -> {
            pwField.setDisable(!newValue);
            nbCharField.clear();
            nbSpecCharField.clear();
            nbNumberField.clear();

            this.pwField.clear();
        });
        pwCheck.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                randomCheck.setSelected(false);
                pwHash();
            }
        });

    }

    public void pwHash() {
        pwField.textProperty().addListener((observable, oldValue, newValue) -> {
            String salt = "sel";
            String hashedPassword = DigestUtils.sha512Hex(salt + newValue);
            resultField.setText(hashedPassword);

        });
    }

    public void numberLimit() {
        nbNumberField.addEventFilter(KeyEvent.KEY_TYPED, event -> {
            // Vérifier si le caractère saisi est un chiffre
            if (!event.getCharacter().matches("[0-9]")) {
                // Si ce n'est pas le cas, consommer l'événement pour l'empêcher d'être traité
                event.consume();
            }
        });
        nbCharField.addEventFilter(KeyEvent.KEY_TYPED, event -> {
            // Vérifier si le caractère saisi est un chiffre
            if (!event.getCharacter().matches("[0-9]")) {
                // Si ce n'est pas le cas, consommer l'événement pour l'empêcher d'être traité
                event.consume();
            }
        });
        nbSpecCharField.addEventFilter(KeyEvent.KEY_TYPED, event -> {
            // Vérifier si le caractère saisi est un chiffre
            if (!event.getCharacter().matches("[0-9]")) {
                // Si ce n'est pas le cas, consommer l'événement pour l'empêcher d'être traité
                event.consume();
            }
        });
    }

    public boolean siteLoginNotEmpty() {
        if (siteField.getText().isEmpty() || loginField.getText().isEmpty()) {
            Alert alert = new Alert(AlertType.NONE);
            alert.setTitle("Saisir le Site ET le Login");
            alert.setHeaderText("Il faut saisir un site et un login");
            alert.setContentText("");
            ButtonType okButton = new ButtonType("Oui", ButtonData.OK_DONE);
            alert.getButtonTypes().addAll(okButton);
            alert.show();
            return false;
        } else
            return true;
    }

    public void show() {
        // Crée la scène et le stage pour la fenêtre du gestionnaire
        // ...

        // Affiche la fenêtre
        // ...
    }

    public void onClose() {
        // Ferme la fenêtre
        Stage stage = (Stage) lblWelcome.getScene().getWindow();
        stage.close();

        // Ouvre la fenêtre du lobby
        // LobbyController lobbyController = new LobbyController();
        // lobbyController.show();
    }

    public ObservableList<Credentials> addCredentials(MouseEvent event) {
        if (siteLoginNotEmpty()) {
            Credentials newCredentials = new Credentials(this.siteField.getText(), this.loginField.getText(),
                    this.pwField.getText());
            this.passwordManagerController.getCredentialsList().add(newCredentials);

            for (Credentials credentials : this.passwordManagerController.getCredentialsList()) {
                System.out.println(
                        credentials.getUrl() + " | " + credentials.getLogin() + " | " + credentials.getPassword());
            }

            this.siteField.clear();
            this.loginField.clear();
            this.pwField.clear();

            save();
        }
        pwHash();
        return this.passwordManagerController.getCredentialsList();
    }

    public void switchToPasswordManagerView() throws IOException {
        // Charge la vue PasswordManager
        FXMLLoader loader = new FXMLLoader(
                getClass().getResource(PasswordManagerController.PASSWORD_MANAGER_VIEW_PATH));
        Parent root = loader.load();

        // Obtient la référence du contrôleur PasswordManager
        // PasswordManagerController passwordManagerController = loader.getController();

        // Affiche la fenêtre du gestionnaire
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();

        // Ferme la fenêtre du lobby
        Stage lobbyStage = (Stage) backButton.getScene().getWindow();
        lobbyStage.close();
    }

    @FXML
    private void backToPasswordManagerController(MouseEvent event) throws IOException {
        switchToPasswordManagerView();
  
    }

    public static String serializeCredentialsList(List<Credentials> listSerializable) {

        Gson gson = new GsonBuilder()
                .create();

        return gson.toJson(listSerializable);

    }

    public static void save() {

        String credentialsFileWriter = serializeCredentialsList(AddCredentialsController.passwordManagerController.getCredentialsList());

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(AddCredentialsController.credentialsFile);
            fileOutputStream.write(credentialsFileWriter.getBytes());
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    public static List<Credentials> deserializeCredentialsList(String credentialsFile) {

        Gson gson = new GsonBuilder()
                .create();

        return Arrays.asList(gson.fromJson(credentialsFile, Credentials[].class));

    }

    public static List<Credentials> loadCredentials(File file) throws IOException {
        String credentialsFile;

        FileInputStream fileInputStream = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new FileReader(file));
        credentialsFile = reader.readLine();
        reader.close();
        fileInputStream.close();

        return deserializeCredentialsList(credentialsFile);
    }
}
