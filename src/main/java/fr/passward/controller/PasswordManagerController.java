package fr.passward.controller;

import java.io.IOException;

import fr.passward.model.Credentials;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Popup;
import javafx.stage.Stage;

public class PasswordManagerController {

    @FXML
    private Label lblWelcome;

    public static final String PASSWORD_MANAGER_VIEW_PATH = "/fr/passward/passwordManager.fxml";

    private ObservableList<Credentials> credentialsList = FXCollections.observableArrayList();

    public ObservableList<Credentials> getCredentialsList() {
        return credentialsList;
    }

    @FXML
    private TableView<Credentials> pwTableView;
    @FXML
    private TableColumn<Credentials, String> loginColumn;
    @FXML
    private TableColumn<Credentials, String> urlColumn;
    @FXML
    private TableColumn<Credentials, String> pwColumn;
    // @FXML
    // private TableColumn<Credentials, Button> optColumn;

    // private final Button eye = new Button();
    @FXML
    private Button addPwButton;
    @FXML
    private Button removePwButton;

    public void show() {
        // Crée la scène et le stage pour la fenêtre du gestionnaire
        // ...

        // Affiche la fenêtre
        // ...
    }

    // Appelé lorsque l'utilisateur ferme la fenêtre du gestionnaire
    public void onClose() {
        // Ferme la fenêtre
        Stage stage = (Stage) lblWelcome.getScene().getWindow();
        stage.close();

        // Ouvre la fenêtre du lobby
        // LobbyController lobbyController = new LobbyController();
        // lobbyController.show();
    }

//     {
//         eye.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("eye-icon.png")))); // Chargement de l'icône œil
//         eye.setOnAction(event -> {
//             Credentials obj = getTableView().getItems().get(getIndex());
//             String password = obj.getPassword();
//             // Afficher le mot de passe ici
//         });
//     }

//     @Override
//     public void updateItem(Void item, boolean empty) {
//         super.updateItem(item, empty);
//         if (empty) {
//             setGraphic(null);
//         } else {
//             setGraphic(btn);
//         }
//     }
// });

    public void initialize() throws IOException {
        // Charge la vue addCredentials
        FXMLLoader loader = new FXMLLoader(getClass().getResource(AddCredentialsController.ADD_CREDENTIALS_VIEW_PATH));
        Parent root = loader.load();

        // Obtient la référence du contrôleur addCredentials
        AddCredentialsController addCredentialsController = loader.getController();
        // Passe une référence à l'instance du PasswordManagerController à
        // AddCredentialsController
        addCredentialsController.setPasswordManagerController(this);

        this.loginColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getLogin()));
        this.urlColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getUrl()));
        this.pwColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPassword()));
        // this.optColumn.setCellValueFactory(cellData -> new
        // SimpleStringProperty(cellData.getValue().getStartTime()));

        // // Récupération de la ligne sélectionnée dans la tableview
        // TablePosition pos = pwTableView.getSelectionModel().getSelectedCells().get(0);
        // int row = pos.getRow();

        // // Récupération de l'objet représentant la ligne sélectionnée
        // Credentials selectedObject = pwTableView.getItems().get(row);

        // // Récupération du mot de passe stocké dans l'objet
        // String password = selectedObject.getPassword();

        credentialsList.add(new Credentials("test", "test", "test"));
        this.pwTableView.setItems(this.credentialsList);
        credentialsList.addAll(AddCredentialsController.loadCredentials(AddCredentialsController.credentialsFile));
    }

    public void switchToAddCredentialsView() throws IOException {
        // Charge la vue addCredentials
        FXMLLoader loader = new FXMLLoader(getClass().getResource(AddCredentialsController.ADD_CREDENTIALS_VIEW_PATH));
        Parent root = loader.load();

        // Obtient la référence du contrôleur addCredentials
        AddCredentialsController addCredentialsController = loader.getController();
        // Passe une référence à l'instance du PasswordManagerController à
        // AddCredentialsController
        addCredentialsController.setPasswordManagerController(this);

        // Affiche la fenêtre du gestionnaire
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();

        // Ferme la fenêtre du lobby
        Stage addCredentials = (Stage) addPwButton.getScene().getWindow();
        addCredentials.close();
    }

    public void removeCredentialsButtonClicked(MouseEvent mouseEvent) throws IOException {
        removeCredentials(mouseEvent);
    }

    public void addCredentialsButtonClicked(MouseEvent mouseEvent) throws IOException {
        // Vérifie les champs renseignes
        // ...

        // Popup popup = new Popup();
        // Label label = new Label("Mon texte de popup !");
        // label.setStyle("-fx-background-color: white; -fx-padding: 10px;");
        // popup.getContent().add(label);
        // popup.setAutoHide(true);

        // FXMLLoader loader = new
        // FXMLLoader(getClass().getResource(AddCredentialsController.ADD_CREDENTIALS_VIEW_PATH));
        // Parent root = loader.load();
        // Stage stage = new Stage();
        // stage.setScene(new Scene(root));
        // stage.show();
        // popup.show(stage , 500, 500);

        // Charge la vue addCredentials si les informations sont valides
        switchToAddCredentialsView();
    }

    public void removeCredentials(MouseEvent event) {
        Credentials selectedCredentials = this.pwTableView.getSelectionModel().getSelectedItem();
        this.credentialsList.remove(selectedCredentials);

        AddCredentialsController.save();
    }
}