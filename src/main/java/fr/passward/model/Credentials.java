package fr.passward.model;

import java.io.Serializable;

import javafx.scene.control.Button;

public class Credentials implements Serializable{
    private String url;
    private String login;
    private String password;
    // private Button opt;

    public Credentials() {
    }

    public Credentials(String url, String login, String password) {
        this.setUrl(url);
        this.setLogin(login);
        this.setPassword(password);
        //this.setOpt(opt);
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "{" +
            " url='" + getUrl() + "'" +
            ", login='" + getLogin() + "'" +
            ", password='" + getPassword() + "'" +
            "}";
    }

    // public Button getOpt() {
    //     return this.opt;
    // }

    // public void setOpt(Button opt) {
    //     this.opt = opt;
    // }
}